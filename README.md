# CLI PHP Tools Readme

* Mixed Content Scan via https://dzone.com/articles/scanning-https-for-mixed-content

## Set up

```
composer global require bramus/mixed-content-scan:~2.8
```

## Add globally-included PHP projects to path

In your `~/.bashrc` or equivalent:

```
export PATH="$HOME/.config/composer/vendor/bramus/mixed-content-scan/bin:$PATH"
```

## Usage

For example for GEO.coop we would use this:

```
mixed-content-scan https://geo.coop/ --output geo-mixed-content-scan.txt
```

Current problem is it is identifying *way* too many pages.  The only ones that are an actual problem for us are the videos and PDFs (including locally-hosted ones) that are supposed to be shown with `<iframe src="http://something` etc. and those silently disappear because for security reasons browsers won't show non-https content within an http site via an iframe.
